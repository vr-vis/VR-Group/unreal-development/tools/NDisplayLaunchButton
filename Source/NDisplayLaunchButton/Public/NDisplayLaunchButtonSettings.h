#pragma once
#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "Engine/DeveloperSettings.h"
#include "NDisplayLaunchButtonSettings.generated.h"

UENUM(BlueprintType)
enum ProjectorDisplayType
{
	DisplayType_Off = 0 UMETA(DisplayName = "Off"),
	DisplayType_Frame_Sequential = 1 UMETA(DisplayName = "Frame Sequential"),
	DisplayType_Side_By_Side = 2 UMETA(DisplayName = "Side By Side"),
	DisplayType_DualHead = 3 UMETA(DisplayName = "Dual Head"),
	DisplayType_Error = 4 UMETA(Hidden)
};

UENUM(BlueprintType)
enum ButtonLaunchType
{
	ButtonLaunchType_NONE UMETA(DisplayName = "Nothing"),
	ButtonLaunchType_MiniCAVE UMETA(DisplayName = "MiniCAVE"),
	ButtonLaunchType_TWO_SCREEN UMETA(DisplayName = "Two Screen"),
	ButtonLaunchType_CAVE UMETA(DisplayName = "CAVE"),
	ButtonLaunchType_ROLV UMETA(DisplayName = "ROLV"),
	ButtonLaunchType_TDW UMETA(DisplayName = "Tiled Display Wall")
};

UCLASS(config=Engine, defaultconfig, meta=(DisplayName="nDisplay Launch Button"))
class UNDisplayLaunchButtonSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, config, Category = "General", meta = (DisplayName = "Start "))
	TEnumAsByte<ButtonLaunchType> LaunchType = ButtonLaunchType_MiniCAVE;

	/*
	 * TwoScreen Options
	 */
	UPROPERTY(EditAnywhere, config, Category = "General|TwoScreen", meta = (DisplayName = "Launch Parameters", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TWO_SCREEN"))
	FString TwoScreenLaunchParameters = "-dc_cluster -dc_dev_mono -windowed -fixedseed -notexturestreaming";
	UPROPERTY(EditAnywhere, config, Category = "General|TwoScreen", meta = (DisplayName = "Additioanl Launch Parameters for Master", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TWO_SCREEN"))
	FString TwoScreenAdditionalLaunchParametersMaster = "";
	UPROPERTY(EditAnywhere, config, Category = "General|TwoScreen|Log", meta = (DisplayName = "Open Log Window for Master Node", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TWO_SCREEN"))
	bool TwoScreenLogMasterWindow = true;
	UPROPERTY(EditAnywhere, config, Category = "General|TwoScreen|Log", meta = (DisplayName = "Write Log for Floor-Left to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TWO_SCREEN"))
	bool TwoScreenLogToProjectDirLeft = true;
	UPROPERTY(EditAnywhere, config, Category = "General|TwoScreen|Log", meta = (DisplayName = "Write Log for Front-Right to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TWO_SCREEN"))
	bool TwoScreenLogToProjectDirRight = false;

	/*
	 * Mini CAVE Options
	 */
	UPROPERTY(EditAnywhere, config, Category = "General|MiniCAVE", meta = (DisplayName = "Launch Parameters", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_MiniCAVE"))
	FString MiniCAVELaunchParameters = "-dc_cluster -dc_dev_mono -windowed -fixedseed -notexturestreaming";
	UPROPERTY(EditAnywhere, config, Category = "General|MiniCAVE", meta = (DisplayName = "Additioanl Launch Parameters for Master", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_MiniCAVE"))
	FString MiniCAVEAdditionalLaunchParametersMaster = "-log";

	UPROPERTY(EditAnywhere, config, Category = "General|MiniCAVE|Log", meta = (DisplayName = "Open Log Window for Master Node", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_MiniCAVE"))
	bool MiniCAVELogMasterWindow = true;
	UPROPERTY(EditAnywhere, config, Category = "General|MiniCAVE|Log", meta = (DisplayName = "Write Log for Floor-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_MiniCAVE"))
	bool MiniCAVELogToProjectDirFloor = true;
	UPROPERTY(EditAnywhere, config, Category = "General|MiniCAVE|Log", meta = (DisplayName = "Write Log for Front-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_MiniCAVE"))
	bool MiniCAVELogToProjectDirFront = false;
	UPROPERTY(EditAnywhere, config, Category = "General|MiniCAVE|Log", meta = (DisplayName = "Write Log for Left-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_MiniCAVE"))
	bool MiniCAVELogToProjectDirLeft = false;
	UPROPERTY(EditAnywhere, config, Category = "General|MiniCAVE|Log", meta = (DisplayName = "Write Log for Right-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_MiniCAVE"))
	bool MiniCAVELogToProjectDirRight = false;
	UPROPERTY(EditAnywhere, config, Category = "General|MiniCAVE|Log", meta = (DisplayName = "Write Log for Back-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_MiniCAVE"))
	bool MiniCAVELogToProjectDirBack = false;

	/*
	 * CAVE Options
	 */
	UPROPERTY(EditAnywhere, config, Category = "General|CAVE", meta = (DisplayName = "Path to Launch Script", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_CAVE"))
	FFilePath CAVELaunchScriptPath;

	/*
	 * ROLV Options
	 */
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV", meta = (DisplayName = "Path to the ROLV nDisplay Config", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV"))
	FFilePath RolvConfig;
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV", meta = (DisplayName = "Launch Parameters", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV"))
	FString ROLVLaunchParameters = "-dc_cluster -nosplash -fixedseed -dx11 -dc_dev_side_by_side -notexturestreaming -fullscreen dc_node=node_main";
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV", meta = (DisplayName = "Write Log to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV"))
	bool ROLVLogToProjectDir = true;

	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|Projector", meta = (DisplayName = "Change Projector Mode on Startup", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV"))
	bool SwitchProjector = true;
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|Projector", meta = (DisplayName = "Switch to ", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV && SwitchProjector"))
	TEnumAsByte<ProjectorDisplayType> ProjectorType;
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|Projector", meta = (DisplayName = "Projector IP", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV && SwitchProjector"))
	FString ProjectorIP;
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|Projector", meta = (DisplayName = "Projector Port", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV && SwitchProjector"))
	int ProjectorPort = 1025;

	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|VRPN", meta = (DisplayName = "Start VRPN in the Background", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV"))
	bool StartVRPN = true;
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|VRPN", meta = (DisplayName = "Path to VRPN Executable", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV && StartVRPN"))
	FFilePath VRPNPath;
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|VRPN", meta = (DisplayName = "Path to VRPN Config", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV && StartVRPN"))
	FFilePath VRPNConfigPath;

	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|DTRACK", meta = (DisplayName = "Start DTrack in the Background", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV"))
	bool StartDTrack = true;
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|DTRACK", meta = (DisplayName = "DTrack IP", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV && StartDTrack"))
	FString DTrackIP;
	UPROPERTY(EditAnywhere, config, Category = "General|ROLV|DTRACK", meta = (DisplayName = "DTrack Port", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_ROLV && StartDTrack"))
	int DTrackPort = 50105;

	/*
	* TiledDisplayWall Options
	*/
	UPROPERTY(EditAnywhere, config, Category = "General|TiledDisplayWall", meta = (DisplayName = "Launch Parameters", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TDW"))
	FString TiledDisplayWallLaunchParameters = "-dc_cluster -dc_dev_mono -windowed -fixedseed -notexturestreaming";
	UPROPERTY(EditAnywhere, config, Category = "General|TiledDisplayWall", meta = (DisplayName = "Additional Launch Parameters for Master", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TDW"))
	FString TiledDisplayWallAdditionalLaunchParametersMaster = "";
	
	UPROPERTY(EditAnywhere, config, Category = "General|TiledDisplayWall|Log", meta = (DisplayName = "Open Log Window for Master Node", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TDW"))
	bool TiledDisplayWallLogMasterWindow = true;
	UPROPERTY(EditAnywhere, config, Category = "General|TiledDisplayWall|Log", meta = (DisplayName = "Write Log for TL-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TDW"))
	bool TiledDisplayWallLogToProjectDirTL = true;
	UPROPERTY(EditAnywhere, config, Category = "General|TiledDisplayWall|Log", meta = (DisplayName = "Write Log for TM-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TDW"))
	bool TiledDisplayWallLogToProjectDirTM = false;
	UPROPERTY(EditAnywhere, config, Category = "General|TiledDisplayWall|Log", meta = (DisplayName = "Write Log for TR-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TDW"))
	bool TiledDisplayWallLogToProjectDirTR = false;
	UPROPERTY(EditAnywhere, config, Category = "General|TiledDisplayWall|Log", meta = (DisplayName = "Write Log for BL-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TDW"))
	bool TiledDisplayWallLogToProjectDirBL = false;
	UPROPERTY(EditAnywhere, config, Category = "General|TiledDisplayWall|Log", meta = (DisplayName = "Write Log for BM-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TDW"))
	bool TiledDisplayWallLogToProjectDirBM = false;
	UPROPERTY(EditAnywhere, config, Category = "General|TiledDisplayWall|Log", meta = (DisplayName = "Write Log for BR-Node to Project Directory", EditCondition="LaunchType==ButtonLaunchType::ButtonLaunchType_TDW"))
	bool TiledDisplayWallLogToProjectDirBR = false;

	/*
	 * Insights Options
	 */
	UPROPERTY(EditAnywhere, config, Category = "General|Insights", meta = (DisplayName = "Unreal Insights "))
	bool bEnableInsights = false;
	UPROPERTY(EditAnywhere, config, Category = "General|Insights", meta = (DisplayName = "Stat Named Events", EditCondition = "bEnableInsights==true"))
	bool bStatNamedEvents = true;
	UPROPERTY(EditAnywhere, config, Category = "General|Insights", meta = (DisplayName = "Tracehost IP", EditCondition = "bEnableInsights==true"))
	FString TracehostIP = "127.0.0.1";

	UPROPERTY(EditAnywhere, config, Category = "General|Insights|Trace Channels", meta = (DisplayName = "Log", EditCondition = "bEnableInsights==true"))
	bool bLog = true;
	UPROPERTY(EditAnywhere, config, Category = "General|Insights|Trace Channels", meta = (DisplayName = "Bookmark", EditCondition = "bEnableInsights==true"))
	bool bBookmark = true;
	UPROPERTY(EditAnywhere, config, Category = "General|Insights|Trace Channels", meta = (DisplayName = "Frame", EditCondition = "bEnableInsights==true"))
	bool bFrame = true;
	UPROPERTY(EditAnywhere, config, Category = "General|Insights|Trace Channels", meta = (DisplayName = "CPU", EditCondition = "bEnableInsights==true"))
	bool bCPU = true;
	UPROPERTY(EditAnywhere, config, Category = "General|Insights|Trace Channels", meta = (DisplayName = "GPU", EditCondition = "bEnableInsights==true"))
	bool bGPU = true;
	UPROPERTY(EditAnywhere, config, Category = "General|Insights|Trace Channels", meta = (DisplayName = "LoadTime", EditCondition = "bEnableInsights==true"))
	bool bLoadTime = true;
	UPROPERTY(EditAnywhere, config, Category = "General|Insights|Trace Channels", meta = (DisplayName = "File", EditCondition = "bEnableInsights==true"))
	bool bFile = true;
	UPROPERTY(EditAnywhere, config, Category = "General|Insights|Trace Channels", meta = (DisplayName = "Net", EditCondition = "bEnableInsights==true"))
	bool bNet = true;
};
