// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "NDisplayLaunchButtonStyle.h"

class FNDisplayLaunchButtonCommands : public TCommands<FNDisplayLaunchButtonCommands>
{
public:

	FNDisplayLaunchButtonCommands()
		: TCommands<FNDisplayLaunchButtonCommands>(TEXT("NDisplayLaunchButton"), NSLOCTEXT("Contexts", "NDisplayLaunchButton", "NDisplayLaunchButton Plugin"), NAME_None, FNDisplayLaunchButtonStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > PluginAction;
};
