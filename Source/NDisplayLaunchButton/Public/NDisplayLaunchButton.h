// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "NDisplayLaunchButtonSettings.h"
#include "Logging/LogMacros.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"

class FToolBarBuilder;
class FMenuBuilder;

DECLARE_LOG_CATEGORY_EXTERN(LogNDisplayLaunchButton, Log, All);

class FNDisplayLaunchButtonModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	bool ChangePluginStateAndStoreConfig(FString PluginName, bool NewState, bool& OldState) const;
	static FString GetEditorExecutableName();
	static FString GetFilePathInProject(FString FileName);
	static FString GetConfigPath(FString ConfigName);
	static void KillProcesses(FProcHandle Processes[], const int Num_Nodes);

	/** This function will be bound to Command. */
	void PluginButtonClicked();

	void SendToDTrack(FString Address, int Port, FString Message);
	ProjectorDisplayType SwitchProjectorToState(FString Address, int Port, ProjectorDisplayType State);
private:
	bool SteamVRState = false;
	bool OculusVRState = false;
};
