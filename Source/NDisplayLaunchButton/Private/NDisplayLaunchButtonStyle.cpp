// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "NDisplayLaunchButtonStyle.h"
#include "NDisplayLaunchButton.h"
#include "Framework/Application/SlateApplication.h"
#include "Styling/SlateStyleRegistry.h"
#include "Slate/SlateGameResources.h"
#include "Interfaces/IPluginManager.h"

TSharedPtr< FSlateStyleSet > FNDisplayLaunchButtonStyle::StyleInstance = NULL;

void FNDisplayLaunchButtonStyle::Initialize()
{
	if (!StyleInstance.IsValid())
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance);
	}
}

void FNDisplayLaunchButtonStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance);
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}

FName FNDisplayLaunchButtonStyle::GetStyleSetName()
{
	static FName StyleSetName(TEXT("NDisplayLaunchButtonStyle"));
	return StyleSetName;
}

TSharedRef< FSlateStyleSet > FNDisplayLaunchButtonStyle::Create()
{
	TSharedRef< FSlateStyleSet > Style = MakeShareable(new FSlateStyleSet("NDisplayLaunchButtonStyle"));
	Style->SetContentRoot(IPluginManager::Get().FindPlugin("NDisplayLaunchButton")->GetBaseDir() / TEXT("Resources"));

	const FVector2D Icon40x40(40.0f, 40.0f);
	Style->Set("NDisplayLaunchButton.PluginAction", new FSlateImageBrush(Style->RootToContentDir(TEXT("NDisplay_40x"), TEXT(".png")), Icon40x40));;
	
	return Style;
}

void FNDisplayLaunchButtonStyle::ReloadTextures()
{
	if (FSlateApplication::IsInitialized())
	{
		FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
	}
}

const ISlateStyle& FNDisplayLaunchButtonStyle::Get()
{
	return *StyleInstance;
}
