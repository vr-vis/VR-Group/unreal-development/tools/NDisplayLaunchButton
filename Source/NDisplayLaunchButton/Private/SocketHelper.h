﻿#pragma once
#include <string>

#include "CoreMinimal.h"
#include "Sockets.h"
#include "Interfaces/IPv4/IPv4Address.h"
#include "Interfaces/IPv4/IPv4Endpoint.h"
#include "Common/TcpSocketBuilder.h"
#include "Logging/LogMacros.h"
#include "SocketHelper.generated.h"

DECLARE_LOG_CATEGORY_CLASS(LogNDisplayLaunchButtonSockerHelper, Log, All);

UCLASS()
class USocketHelper : public UObject{
	GENERATED_BODY()

public:
	
	static FSocket* OpenSocket(FString Address, int Port, FString SocketName) {
		FIPv4Address ParsedAddress;
		if (!FIPv4Address::Parse(Address, ParsedAddress)) {
			UE_LOG(LogNDisplayLaunchButtonSockerHelper, Error, TEXT("Could not parse Address %s"), *Address);
			return nullptr;
		}

		FIPv4Endpoint Endpoint(ParsedAddress, Port);
		FSocket* Socket = FTcpSocketBuilder(*SocketName);
		if (!Socket->Connect(Endpoint.ToInternetAddr().Get()))
		{
			UE_LOG(LogNDisplayLaunchButtonSockerHelper, Error, TEXT("Error connecting to server %s:%d via %s"), *Address, Port, *SocketName);
			Socket->Close();
			return nullptr;
		}
		return Socket;
	}

	static int32 SendSocket(FSocket* Socket, FString Message) {
		Socket->Wait(ESocketWaitConditions::WaitForWrite, FTimespan::FromSeconds(5));
		std::string Buffer = std::string(TCHAR_TO_UTF8(*Message));
		int32 Sent = 0;
		if (!Socket->Send(reinterpret_cast<const uint8 *>(Buffer.c_str()), Buffer.length(), Sent))
		{
			UE_LOG(LogNDisplayLaunchButtonSockerHelper, Error, TEXT("Error sending via %s. Sent %d bytes"), *Socket->GetDescription(), Sent);
			Socket->Close();
		}
		return Sent;
	}
	
	template<int32 BufferSize> static FString ReceiveSocket(FSocket* Socket) {
		Socket->Wait(ESocketWaitConditions::WaitForRead, FTimespan::FromSeconds(5));
		char ReceiveBuffer[BufferSize];
		int BytesReceived = 0;
		if (!Socket->Recv(reinterpret_cast<uint8*>(&ReceiveBuffer), BufferSize, BytesReceived)) {
			UE_LOG(LogNDisplayLaunchButtonSockerHelper, Error, TEXT("No valid response from %s. Response: '%s'"), *Socket->GetDescription(), *FString(std::string(ReceiveBuffer, BytesReceived).c_str()));
			Socket->Close();
		}
		if (BytesReceived <= 0) return FString("");
		return FString(std::string(ReceiveBuffer, BytesReceived).c_str());
	}
	
	static void CloseSocket(FSocket* Socket) {
		Socket->Shutdown(ESocketShutdownMode::ReadWrite);
		Socket->Close();
	}
};
