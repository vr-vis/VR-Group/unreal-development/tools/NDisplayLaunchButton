// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "NDisplayLaunchButtonCommands.h"
#include "NDisplayLaunchButtonSettings.h"

#define LOCTEXT_NAMESPACE "FNDisplayLaunchButtonModule"

void FNDisplayLaunchButtonCommands::RegisterCommands()
{
	UI_COMMAND(PluginAction, "N-Display", "Launch current project via N-Display", EUserInterfaceActionType::Button, FInputChord());
}

#undef LOCTEXT_NAMESPACE
