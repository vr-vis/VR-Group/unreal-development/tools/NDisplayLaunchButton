# What is it good for?
This plugin adds an extra button besides the normal 'Play'-button in the Editor toolbar. This button automatically launches your project in the nDisplay mode.
For this 4[+1] modes are available:
* Two Screen - Opens two windows that side by side show an nDisplay view of your desktop
* MiniCAVE - Launches a folded open miniature cave on you screen to test CAVE related stuff in on your desktop
* CAVE - Use this button on your CAVE master to automatically start the demo from the editor directly in the CAVE
* ROLV - Use this button on your ROLV to automatically start the demo from the editor
    * Supports DTrack starting, VRPN starting and Projector-3D-Mode switching
* [None] - The button does nothing
# How to use this plugin:
1. Add it as a submodule to your project by executing `git submodule add https://devhub.vr.rwth-aachen.de/VR-Group/NDisplayLaunchButton.git Plugins/NDisplayLaunchButton` from your projects root.
2. Execute the "Generate Project Files" for your project
3. Build your project via VisualStudio
4. Open your project
5. Go to Edit -> Plugins -> Built-In -> Virtual Reality and disable OculusVR and SteamVR
6. Restart the Editor
7. Start the Project in normal Standalone Mode to start shader compilation and wait until all shaders are compiled
8. You should see the newly added NDisplay Launch Button beside the normal Play Button. Hit it!

### Options:
In Edit -> Project Settings -> Engine -> nDisplay Launch Button you can find the options of the plugin
You can specify logging options and launch parameters in there.
